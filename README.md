# Tiling grid for Sentinel-2 Level-1C products

Sentinel-2 products are a compilation of elementary granules of fixed size, along with a single orbit. A granule is the minimum indivisible partition of a product (containing all possible spectral bands). For Level-1C and Level-2A, the granules, also called tiles, are 10,000 km2 ortho-images in UTM/WGS84 projection.

A kml file of the complete Grid can be found on the [data-product page](https://sentinel.esa.int/web/sentinel/missions/sentinel-2/data-products) of sentinel.esa.web

This repo has Sentinel 2 Tilling Grid as GeoJson (Spain)

![Sentinel 2 Tilling Grid Spain](sentinel2gridSpain.png)

Military Grid Reference System (MGRS)

Data Source: http://earth-info.nga.mil/GandG/coordsys/grids/mgrs_100km_dloads.html#Downloading
